package main

import (
	"log"
	"net/http"
)

func home(writer http.ResponseWriter, request *http.Request) {
	if request.URL.Path != "/" {
		http.NotFound(writer, request)
		return
	}

	writer.Write([]byte("Hello from Snippetbox!"))
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func snippetView(writer http.ResponseWriter, request *http.Request) {
	writer.Write([]byte("Display a specific snippet..."))
}

func snippetCreate(writer http.ResponseWriter, request *http.Request) {
	writer.Write([]byte("Create a new snippet..."))
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", home)
	mux.HandleFunc("/snippet/view", snippetView)
	mux.HandleFunc("/snippet/create", snippetCreate)

	log.Print(("Starting server on :4000"))
	err := http.ListenAndServe(":4000", mux)
	check(err)
}
